import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';

import websocketsMiddleware from './middleware/websockets';
import reducers from './reducers';

import './index.css';
import App from './components/App';
import * as serviceWorker from './serviceWorker';

const storeMiddleware = compose(
  applyMiddleware(websocketsMiddleware),
  // eslint-disable-next-line no-underscore-dangle
  ...(window.__REDUX_DEVTOOLS_EXTENSION__ ? [window.__REDUX_DEVTOOLS_EXTENSION__()] : []),
);

const store = createStore(
  reducers,
  storeMiddleware,
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
