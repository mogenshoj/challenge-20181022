import {
  WEBSOCKET_OPEN,
  WEBSOCKET_CLOSE,
  WEBSOCKET_SUBSCRIBE,
  WEBSOCKET_SEND,
} from '../constants';

const reducer = (state = {
  open: false,
  subscriptions: {},
}, action) => {
  switch (action.type) {
    case WEBSOCKET_OPEN:
      return {
        ...state,
        open: true,
      };
    case WEBSOCKET_CLOSE:
      return {
        ...state,
        open: false,
      };
    case WEBSOCKET_SUBSCRIBE:
      return {
        ...state,
        subscriptions: {
          ...state.subscriptions,
          [action.payload.chanId]: action.payload,
        },
      };
    case WEBSOCKET_SEND:
      if (action.payload.event === 'unsubscribe') {
        // we need to remove our saved subscription
        let {
          [String(action.payload.chanId)]: omit,
          ...res
        } = state.subscriptions;

        return {
          ...state,
          subscriptions: res,
        };
      }
      return state;
    default:
      return state
  }
}

export default reducer;
