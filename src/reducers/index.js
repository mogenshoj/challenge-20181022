import { combineReducers } from 'redux';

import orderbook from './orderbook';
import websocket from './websocket';
import trades from './trades';
import ticker from './ticker';

export default combineReducers({
  orderbook,
  websocket,
  trades,
  ticker,
});
