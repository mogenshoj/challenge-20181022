import {
  ORDERBOOK_MESSAGE,
} from '../constants';

const initialState = {
  bid: [],
  ask: [],
};


const book = (state = {
  amount: null,
  price: null,
  count: null,
}, action) => {
  switch (action.type) {
    case 'ADD':
      const [price, count, amount] = action.payload;
      return {
        amount,
        price,
        count,
      };
    default:
      return state;
  }
};

const handleUpdateItem = (state, newItem) => {
  const bookKey = newItem.amount < 0 ? 'ask' : 'bid';
  if (newItem.count === 0) {
    // we need to remove it
    return {
      ...state,
      [bookKey]: state[bookKey].filter(i => i.price !== newItem.price),
    }
  }

  let foundItem = false;

  let newState = {
    ...state,
    [bookKey]: state[bookKey].map(b => {
      if (b.price === newItem.price) {
        foundItem = true;
        return newItem;
      } else {
        return b;
      }
    }),
  };

  if (!foundItem) {
    // we haven't found the item, we need to add it.
    // could have done this prettier inside the same loop and not having to sort every time...
    const sortByPrice = (a,b) => (b.price > a.price) ? 1 : ((a.price > b.price) ? -1 : 0);
    newState = {
      ...newState,
      [bookKey]: newState[bookKey].concat([newItem]).sort(sortByPrice),
    };
  }

  return newState;
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ORDERBOOK_MESSAGE:
      // remove the channel id
      const data = [].concat(action.payload.slice(1));

      if (Array.isArray(data[0])) {
        // first data

        return data[0].reduce((all, current) => {
          const item = book(null, {
            type: 'ADD',
            payload: current
          });

          return {
            bid: [...all.bid].concat(item.amount > 0 ? [item] : []),
            ask: [...all.ask].concat(item.amount < 0 ? [item] : []),
          }
        }, initialState);
      } else {
        const updateItem = book(null, {
          type: 'ADD',
          payload: data
        });

        if (isNaN(updateItem.price)) {
          // ignore if there is no price (e.g. hb)
          return state;
        }
        return handleUpdateItem(state, updateItem);
      }
    default:
      return state
  }
}

export default reducer;
