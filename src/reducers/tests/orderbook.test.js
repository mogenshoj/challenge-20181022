import reducer from '../orderbook';
import * as types from '../../constants';

const initialLoad = [41,[[6549.1,13,30.25160284],[6549,1,0.50162222],[6557.6,1,-0.4],[6558,1,-10]]];
const messageUpdate = [41, 6557.6, 2, -2];
const messageRemove = [41, 6557.6, 0, -2];

describe('orderbook reducer', () => {

  it(`should handle ${types.ORDERBOOK_MESSAGE} intial load`, () => {
    expect(
      reducer({}, {
        type: types.ORDERBOOK_MESSAGE,
        payload: initialLoad,
      }),
    ).toEqual({
      ask: [
        {
          amount: -0.4,
          count: 1,
          price: 6557.6,
        },
        {
          amount: -10,
          count: 1,
          price: 6558,
        },
       ],
     bid: [
        {
          amount: 30.25160284,
          count: 13,
          price: 6549.1,
        },
        {
          amount: 0.50162222,
          count: 1,
          price: 6549,
        },
     ],
    });
  });

  it(`should handle ${types.ORDERBOOK_MESSAGE} updates`, () => {
    const state = reducer({}, {
      type: types.ORDERBOOK_MESSAGE,
      payload: initialLoad,
    });

    // it should update the amount and count for the given price point
    expect(
      reducer(state, {
        type: types.ORDERBOOK_MESSAGE,
        payload: messageUpdate,
      }),
    ).toEqual({
      ask: [
        {
          amount: -2, // updated
          count: 2, // updated
          price: 6557.6,
        },
        {
          amount: -10,
          count: 1,
          price: 6558,
        },
       ],
     bid: [
        {
          amount: 30.25160284,
          count: 13,
          price: 6549.1,
        },
        {
          amount: 0.50162222,
          count: 1,
          price: 6549,
        },
     ],
    });
  });

  it(`should handle ${types.ORDERBOOK_MESSAGE} with removals`, () => {
    const state = reducer({}, {
      type: types.ORDERBOOK_MESSAGE,
      payload: initialLoad,
    });

    // it should remove the ask given count = 0
    expect(
      reducer(state, {
        type: types.ORDERBOOK_MESSAGE,
        payload: messageRemove,
      }),
    ).toEqual({
      ask: [
        {
          amount: -10,
          count: 1,
          price: 6558,
        },
       ],
     bid: [
        {
          amount: 30.25160284,
          count: 13,
          price: 6549.1,
        },
        {
          amount: 0.50162222,
          count: 1,
          price: 6549,
        },
     ],
    });
  });
});
