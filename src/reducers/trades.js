import {
  TRADES_MESSAGE,
} from '../constants';

const trade = (state = {
  id: null,
  type: null,
  amount: null,
  price: null,
  timestamp: null,
}, action) => {
  switch (action.type) {
    case 'ADD':
      const [type, seq, id, timestamp, price, amount, diff] = action.payload;
      return {
        seq,
        id,
        type,
        timestamp,
        price,
        amount,
        diff,
      };
    default:
      return state;
  }
};

const reducer = (state = [], action) => {
  switch (action.type) {
    case TRADES_MESSAGE:
      const newTrade = trade(null, {
        type: 'ADD',
        payload: action.payload.slice(1), // remove channel id
      });

      // skip if there is not price or the type is not 'tu' (not sure how to handle te)
      if (isNaN(newTrade.price) || newTrade.type !== 'tu') {
        return state;
      }

      // only show max 10 last trades
      return [newTrade].concat(state.slice(0, 9));
    default:
      return state
  }
}

export default reducer;
