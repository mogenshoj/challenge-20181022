import {
  TICKER_MESSAGE,
} from '../constants';

const reducer = (state = {
  bid: null,
  bidSize: null,
  ask: null,
  askSize: null,
  dailyChange: null,
  dailyChangePerc: null,
  lastPrice: null,
  volume: null,
  high: null,
  low: null,
}, action) => {
  switch (action.type) {
    case TICKER_MESSAGE:
      if (action.payload[1] === 'hb') {
        return state;
      }

      const [
        chanId, // eslint-disable-line no-unused-vars
        bid,
        bidSize,
        ask,
        askSize,
        dailyChange,
        dailyChangePerc,
        lastPrice,
        volume,
        high,
        low,
      ] = action.payload;

      return {
        bid,
        bidSize,
        ask,
        askSize,
        dailyChange,
        dailyChangePerc,
        lastPrice,
        volume,
        high,
        low,
      };
    default:
      return state
  }
}

export default reducer;
