import PropTypes from 'prop-types';

export const propTrade = {
  id: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  timestamp: PropTypes.number.isRequired,
};

export const propSubscription = {
  chanId: PropTypes.number.isRequired,
  channel: PropTypes.string.isRequired,
  event: PropTypes.string.isRequired,
  pair: PropTypes.string.isRequired,
};

export const propTicker = {
  ask: PropTypes.number,
  askSize: PropTypes.number,
  bid: PropTypes.number,
  bidSize: PropTypes.number,
  dailyChange: PropTypes.number,
  dailyChangePerc: PropTypes.number,
  high: PropTypes.number,
  lastPrice: PropTypes.number,
  low: PropTypes.number,
  volume: PropTypes.number,
};

export const propBook = {
  amount: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
};
