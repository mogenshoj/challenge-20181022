import React from 'react';
import { shallow, mount } from 'enzyme';

import { Trades } from '../Trades';

describe('<Trades />', () => {
  const mockedSubscribe = jest.fn();
  const mockedUnsubscribe = jest.fn();

  const mockedSubscription = {
    chanId: 1,
    channel: 'trades',
    event: 'subscribed',
    pair: 'BTCUSD',
  };

  const mockedTrades = [{
    id: 1,
    type: 'tu',
    amount: 1,
    price: 2,
    timestamp: 1540216454,
  }, {
    id: 2,
    type: 'tu',
    amount: 2,
    price: 5,
    timestamp: 1540216453,
  }];

  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <Trades
        subscribe={mockedSubscribe}
        unsubscribe={mockedUnsubscribe}
        subscription={mockedSubscription}
        trades={mockedTrades}
      />,
    );
  });

  it('should call subscribe on mount', () => {
    expect(mockedSubscribe).toHaveBeenCalledTimes(1);
  });

  it('should call unsubscribe on unmount', () => {
    wrapper = mount(
      <Trades
        subscribe={mockedSubscribe}
        unsubscribe={mockedUnsubscribe}
        subscription={mockedSubscription}
        trades={mockedTrades}
      />,
    );
    wrapper.unmount();

    expect(mockedUnsubscribe).toHaveBeenCalledTimes(1);
  });

  it(`should list ${mockedTrades.length} trades`, () => {
    // expect to show the trades
    expect(wrapper.find('tbody').find('tr')).toHaveLength(2);
  });

  it('should show a message if there are no trades', () => {
    wrapper = shallow(
      <Trades
        subscribe={mockedSubscribe}
        unsubscribe={mockedUnsubscribe}
        subscription={mockedSubscription}
        trades={[]}
      />,
    );

    // the error message should be there (should add a class to it for selecting)
    expect(wrapper.find('strong').exists()).toBe(true);

    // the table should not be there
    expect(wrapper.find('table').exists()).toBe(false);
  });
});
