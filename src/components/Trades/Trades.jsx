import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { propTrade, propSubscription } from '../../propTypes';

import { subscribeToTrades } from '../../actions/trades';
import { unsubscribe } from '../../actions/websocket';
import { getTrades } from '../../selectors/trades';
import { getSubscription } from '../../selectors/websocket';

import styles from './Trades.module.css';

const formatTime = (timestamp) => {
  var date = new Date(timestamp * 1000);
  return date.toLocaleTimeString(navigator.language, {
    hour: '2-digit',
    minute:'2-digit',
    second: '2-digit',
  });
}

export class Trades extends React.Component {
  componentDidMount() {
    const { subscribe, pair } = this.props;

    subscribe(pair);
  }

  componentWillUnmount() {
    const { subscription, unsubscribe } = this.props;

    if (subscription) {
      unsubscribe(subscription.chanId);
    }
  }

  renderRows(trades) {
    return trades.map(t => {
      const isAsk = t.amount < 0;
      const amount = Math.abs(t.amount);

      return (
        <tr key={t.id} className={isAsk ? styles.isAsk : null}>
          <td>{formatTime(t.timestamp)}</td>
          <td>{t.price}</td>
          <td>{amount}</td>
        </tr>
      );
    });
  }

  render() {
    const { trades } = this.props;

    if (trades.length === 0) {
      return <strong>There are no trades yet</strong>;
    }

    return (
      <div>
        <table>
          <thead>
            <tr>
              <th>Time</th>
              <th>Price</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            {this.renderRows(trades)}
          </tbody>
        </table>
      </div>
    );
  }
}

Trades.propTypes = {
  trades: PropTypes.arrayOf(PropTypes.shape(propTrade)).isRequired,
  subscription: PropTypes.shape(propSubscription),
  subscribe: PropTypes.func.isRequired,
  unsubscribe: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    trades: getTrades(state),
    subscription: getSubscription(state, 'trades'),
  }),
  dispatch => ({
    subscribe: pair => dispatch(subscribeToTrades(pair)),
    unsubscribe: chanId => dispatch(unsubscribe(chanId)),
  }),
)(Trades);

