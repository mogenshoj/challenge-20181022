import React from 'react';
import PropTypes from 'prop-types';

import { propBook } from '../../propTypes';

import { roundNumber } from '../../utils/numberFormat';
import styles from './OrderBook.module.css';

class OrderBookTable extends React.Component {
  renderRows(book) {
    let total = 0;

    return book.map(b => {
      let { price, count, amount } = b;
      amount = Math.abs(amount);
      total += amount;
      return (
        <tr key={price}>
          <td>{count}</td>
          <td>{roundNumber(amount, 2)}</td>
          <td>{roundNumber(total, 2)}</td>
          <td>{roundNumber(price, 2)}</td>
        </tr>
      )
    });
  }

  render() {
    const { book } = this.props;
    return (
      <table className={styles.bookTable}>
        <thead>
          <tr>
            <th>Count</th>
            <th>Amount</th>
            <th>Total</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {this.renderRows(book)}
        </tbody>
      </table>
    );
  }
}

OrderBookTable.propTypes = {
  book: PropTypes.arrayOf(PropTypes.shape(propBook)),
};

export default OrderBookTable;

