import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import OrderBookTable from './Table';
import Precision from './Controls/Precision';

import { propBook, propSubscription } from '../../propTypes';

import { subscribeToOrderBook } from '../../actions/orderbook';
import { unsubscribe } from '../../actions/websocket';

import { getOrderbook } from '../../selectors/orderbook';
import { getSubscription } from '../../selectors/websocket';

import styles from './OrderBook.module.css';

class OrderBook extends React.Component {
  componentDidMount() {
    const { subscribe, pair } = this.props;

    subscribe(pair);
  }

  componentWillUnmount() {
    this.handleUnsubscribe();
  }

  handleChangePrecision = (newPrecision) => {
    const { subscribe, subscription } = this.props;

    this.handleUnsubscribe();
    subscribe(subscription.pair, newPrecision);
  }

  handleUnsubscribe = () => {
    const { unsubscribe, subscription } = this.props;
    if (subscription) {
      unsubscribe(subscription.chanId);
    }
  }

  render() {
    const { subscription, book } = this.props;

    return (
      <div>
        <Precision
          subscription={subscription}
          onChange={this.handleChangePrecision}
        />

        <div className={styles.bookWrapper}>
          <OrderBookTable book={book.bid} />
          <OrderBookTable book={book.ask} />
        </div>
      </div>
    );
  }
}

OrderBook.propTypes = {
  book: PropTypes.shape({
    ask: PropTypes.arrayOf(PropTypes.shape(propBook)),
    bid: PropTypes.arrayOf(PropTypes.shape(propBook)),
  }).isRequired,
  subscription: PropTypes.shape(propSubscription),
  subscribe: PropTypes.func.isRequired,
  unsubscribe: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    subscription: getSubscription(state, 'book'),
    book: getOrderbook(state),
  }),
  dispatch => ({
    subscribe: (pair, prec) => dispatch(subscribeToOrderBook(pair, prec)),
    unsubscribe: chanId => dispatch(unsubscribe(chanId)),
  }),
)(OrderBook);

