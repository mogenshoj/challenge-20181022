import React from 'react';

import PropTypes from 'prop-types';
import { propSubscription } from '../../../propTypes';

const precisions = ['P0', 'P1', 'P2', 'P3'];
class Precision extends React.Component {
  handleChangePrecision = (direction) => {
    const { onChange, subscription } = this.props;

    const currentIndex = precisions.indexOf(subscription.prec);
    const nextIndex = currentIndex + direction;

    if (precisions[nextIndex]) {
      onChange(precisions[nextIndex])
    }
  }

  render() {
    const { subscription } = this.props;

    return (
      <div>
        <label>Precision</label>
        {' '}
        <button
          onClick={() => this.handleChangePrecision(-1)}
          disabled={subscription && subscription.prec === precisions[0]}
        >-</button>
        <button
          onClick={() => this.handleChangePrecision(1)}
          disabled={subscription && subscription.prec === precisions[precisions.length - 1]}
        >+</button>
      </div>
    );
  }
};

Precision.defaultProps = {
  subscription: {},
};

Precision.propTypes = {
  subscription: PropTypes.shape(propSubscription),
  onChange: PropTypes.func.isRequired,
};

export default Precision;
