import React from 'react';

import { connect } from 'react-redux';

import { connect as websocketConnect, disconnect } from '../actions/websocket';

import OrderBook from './OrderBook/OrderBook';
import Trades from './Trades/Trades';
import Ticker from './Ticker/Ticker';
import styles from './App.module.css';

class App extends React.Component {
  componentDidMount() {
    this.handleWebsocketConnect();
  }

  handleWebsocketConnect = () => {
    const { connectToWebsocket } = this.props;
    connectToWebsocket();
  }

  handleWebsocketDisconnect = () => {
    const { disconnectFromWebsocket } = this.props;
    disconnectFromWebsocket();
  }

  render() {
    const { websocketConnected } = this.props;

    const pair = 'BTCUSD';

    return (
      <div>
        {!websocketConnected && (
          <strong>You are not connected!</strong>
        )}

        <div>
          {!websocketConnected && <button onClick={this.handleWebsocketConnect}>Connect</button>}
          {websocketConnected && <button onClick={this.handleWebsocketDisconnect}>Disconnect</button>}
        </div>

        {websocketConnected && (
          <div className={styles.App}>
            <div className={styles.ticker}>
              <h2>Ticker</h2>
              <Ticker pair={pair} />
            </div>

            <div className={styles.trades}>
              <h2>Trades</h2>
              <Trades pair={pair} />
            </div>

            <div className={styles.orderbook}>
              <h2>Order book</h2>
              <OrderBook pair={pair} />
            </div>
          </div>
        )}
      </div>
    );
  }
}


export default connect(
  state => ({
    websocketConnected: state.websocket.open,
  }),
  dispatch => ({
    connectToWebsocket: () => dispatch(websocketConnect()),
    disconnectFromWebsocket: () => dispatch(disconnect()),
  }),
)(App);

