import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { propTicker, propSubscription } from '../../propTypes';

import { roundNumber } from '../../utils/numberFormat';
import { subscribeToTicker } from '../../actions/ticker';
import { unsubscribe } from '../../actions/websocket';
import { getTicker } from '../../selectors/ticker';
import { getSubscription } from '../../selectors/websocket';

import styles from './Ticker.module.css';

class Ticker extends React.Component {
  componentDidMount() {
    const { subscribe, pair } = this.props;

    subscribe(pair);
  }

  componentWillUnmount() {
    const { subscription, unsubscribe } = this.props;
    if (subscription) {
      unsubscribe(subscription.chanId);
    }
  }

  render() {
    const { ticker, pair } = this.props;

    return (
      <div>
        <table>
          <tbody>
            <tr>
              <td>{pair}</td>
              <td>{roundNumber(ticker.lastPrice, 1)}</td>
            </tr>
            <tr>
              <td>
                <span>Vol: {roundNumber(ticker.volume, 1)} BTC</span>
                <br />
                <span>Low: {roundNumber(ticker.low, 1)}</span>
              </td>
              <td>
                <span className={ticker.dailyChangePerc < 0 ? styles.negPrice : null}>
                  {Math.abs(roundNumber(ticker.lastPrice * ticker.dailyChangePerc, 2))}
                  {' '}
                  ({ticker.dailyChangePerc * 100}%)
                </span>
                <br />
                <span>High: {roundNumber(ticker.high, 1)}</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

Ticker.propTypes = {
  ticker: PropTypes.shape(propTicker).isRequired,
  subscription: PropTypes.shape(propSubscription),
  subscribe: PropTypes.func.isRequired,
  unsubscribe: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    ticker: getTicker(state),
    subscription: getSubscription(state, 'ticker'),
  }),
  dispatch => ({
    subscribe: pair => dispatch(subscribeToTicker(pair)),
    unsubscribe: chanId => dispatch(unsubscribe(chanId)),
  }),
)(Ticker);

