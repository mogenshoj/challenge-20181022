import {
  WEBSOCKET_CONNECT,
  WEBSOCKET_OPEN,
  WEBSOCKET_CLOSE,
  WEBSOCKET_SEND,
  WEBSOCKET_DISCONNECT,
  WEBSOCKET_SUBSCRIBE,
  ORDERBOOK_MESSAGE,
  TRADES_MESSAGE,
  TICKER_MESSAGE,
} from '../constants';

import { getChannel } from '../selectors/websocket';

let websocket;

const findMessageAction = (state, chanId) => {
  const channel = getChannel(state, chanId);
  switch(channel) {
    case 'book':
      return ORDERBOOK_MESSAGE;
    case 'trades':
      return TRADES_MESSAGE;
    case 'ticker':
      return TICKER_MESSAGE;
    default:
  }
}

const middleware = store => next => action => {
  switch (action.type) {
    case WEBSOCKET_CONNECT:
      websocket = new WebSocket(action.payload.url);

      websocket.onopen = () => store.dispatch({ type: WEBSOCKET_OPEN });
      websocket.onclose = (event) => store.dispatch({ type: WEBSOCKET_CLOSE, payload: event });
      websocket.onmessage = (event) => {
        const data = JSON.parse(event.data);

        if (data.event === 'subscribed') {
          store.dispatch({ type: WEBSOCKET_SUBSCRIBE, payload: data })
        } else {
          const messageAction = findMessageAction(store.getState(), data[0]);
          if (messageAction) {
            store.dispatch({ type: messageAction, payload: data })
          }
        }
      };

      break;

    case WEBSOCKET_SEND:
      try {
        websocket.send(JSON.stringify(action.payload));
      } catch (e) {
        console.log('Error sending websocket request', e); // eslint-disable-line no-console
      }

      break;

    case WEBSOCKET_DISCONNECT:
      websocket.close();
      break;

    default:
      break;
  };

  return next(action);
};

export default middleware;
