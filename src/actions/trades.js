import { WEBSOCKET_SEND } from '../constants';

export const subscribeToTrades = (pair) => ({
  type: WEBSOCKET_SEND,
  payload: {
    event: 'subscribe',
    channel: 'trades',
    pair,
  },
});
