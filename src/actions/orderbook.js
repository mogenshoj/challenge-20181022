import { WEBSOCKET_SEND } from '../constants';

export const subscribeToOrderBook = (pair, prec = 'P0') => ({
  type: WEBSOCKET_SEND,
  payload: {
    event: 'subscribe',
    channel: 'book',
    symbol: `t${pair}`,
    prec,
  },
});
