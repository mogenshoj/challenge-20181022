import { WEBSOCKET_SEND } from '../constants';

export const subscribeToTicker = (pair) => ({
  type: WEBSOCKET_SEND,
  payload: {
    event: 'subscribe',
    channel: 'ticker',
    pair,
  },
});
