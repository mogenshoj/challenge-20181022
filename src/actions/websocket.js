import {
  WEBSOCKET_CONNECT,
  WEBSOCKET_DISCONNECT,
  WEBSOCKET_SEND,
} from '../constants';

export const connect = (url = 'wss://api.bitfinex.com/ws/') => ({
  type: WEBSOCKET_CONNECT,
  payload: { url }
});

export const disconnect = () => ({
  type: WEBSOCKET_DISCONNECT,
});

export const unsubscribe = (chanId) => ({
  type: WEBSOCKET_SEND,
  payload: {
    event: 'unsubscribe',
    chanId,
  },
});
