export const roundNumber = (number, digits = 2) => {
  const rounding = Math.pow(10, digits);
  return Math.round(number * rounding) / rounding;
};
