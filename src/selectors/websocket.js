export const getChannel = (state, chanId) => {
  if (state.websocket.subscriptions[chanId]) {
    return state.websocket.subscriptions[chanId].channel;
  }
};

export const getSubscription = (state, channel) => {
  const channelId = Object.keys(state.websocket.subscriptions)
    .find(chanId => state.websocket.subscriptions[chanId].channel === channel);

  return channelId ? state.websocket.subscriptions[channelId] : null;
};
