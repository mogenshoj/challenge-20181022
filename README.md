# Code challenge

## To start
Run `npm install` to install dependencies

Run `npm start` to start the application

## Missed things due to time constraints
* Didn't have time to do the depth charts
* Very barebones styling
* Very limited testing (one component test, one reducer test)
